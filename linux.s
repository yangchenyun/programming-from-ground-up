# PROGRAM CONSTANTS
.equ SYSCALL, 0x80
.equ SYS_EXIT, 1
.equ SYS_READ, 3
.equ SYS_WRITE, 4
.equ SYS_OPEN, 5
.equ SYS_CLOSE, 6
.equ SYS_BRK, 45
.equ EOF, 0

# STANDARD FILE DESCRIPTOR
.equ STDIN, 0
.equ STDOUT, 1
.equ STDERR, 2

# MAIN PROGRAM STACK OFFSET
.equ ST_ARGC, 0     # number of arguments
.equ ST_ARGV_0, 4   # name of program
.equ ST_ARGV_1, 8   # first argument
.equ ST_ARGV_2, 12  # second argument
.equ ST_ARGV_3, 16  # third argument
.equ ST_ARGV_4, 20  # forth argument
.equ ST_ARGV_5, 24  # fifth argument
.equ ST_ARGV_6, 28  # sixth argument
.equ ST_ARGV_7, 32  # seventh argument
.equ ST_ARGV_8, 36  # eighth argument
