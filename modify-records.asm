.include "linux.s"
.include "record-def.s"
# PURPOSE: Read records from file, increment its age and write back to file

.section .data
input_file_name:
.ascii "test.dat\0"

output_file_name:
.ascii "testout.dat\0"

.section .bss
.lcomm BUFFER, RECORD_SIZE

.section .text
.equ ST_IN_FD, -4
.equ ST_OUT_FD, -8 # need to separate fd for reading and writing

.global _start
_start:
movl %esp, %ebp
subl $8, %esp # reserve stack space for both input and output file descriptor

open_file:
movl $SYS_OPEN, %eax
movl $input_file_name, %ebx
movl $0, %ecx  # O_RDONLY
movl $0666, %edx
int $SYSCALL
movl %eax, ST_IN_FD(%ebp)

movl $SYS_OPEN, %eax
movl $output_file_name, %ebx
movl $0101, %ecx  # O_WRONLY & O_CREAT
movl $0666, %edx
int $SYSCALL
movl %eax, ST_OUT_FD(%ebp)

read_loop:
# read record
pushl ST_IN_FD(%ebp)
pushl $BUFFER
call read_record
addl $8, %esp

cmpl $RECORD_SIZE, %eax
jne exit # return when we are not getting a full record

incl RECORD_AGE + BUFFER

# write record
pushl ST_OUT_FD(%ebp)
pushl $BUFFER
call write_record
addl $8, %esp

jmp read_loop

exit:
close_file:
movl $SYS_CLOSE, %eax
movl ST_IN_FD(%ebp), %ebx
int $SYSCALL

movl $SYS_CLOSE, %eax
movl ST_OUT_FD(%ebp), %ebx
int $SYSCALL

movl $SYS_EXIT, %eax
movl $0, %ebx
int $SYSCALL
