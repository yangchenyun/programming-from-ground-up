.include "linux.s"
.include "record-def.s"
# PURPOSE: Find the largest age in the given records.

.section .data
input_file_name:
.ascii "test.dat\0"

.section .bss
.lcomm BUFFER, RECORD_SIZE

.section .text
.equ ST_IN_FD, -4
.equ MAX_AGE, -8

.global _start
_start:
movl %esp, %ebp
subl $8, %esp # reserve stack space for input file descriptor and MAX_AGE

movl $0, MAX_AGE(%ebp)
movl $0, %ecx # %ecx stores the current age

open_file:
movl $SYS_OPEN, %eax
movl $input_file_name, %ebx
movl $0, %ecx  # O_RDONLY
movl $0666, %edx
int $SYSCALL
movl %eax, ST_IN_FD(%ebp)

read_loop:
# read record
pushl ST_IN_FD(%ebp)
pushl $BUFFER
call read_record
addl $8, %esp

cmpl $RECORD_SIZE, %eax
jne exit # return when we are not getting a full record


movl RECORD_AGE + BUFFER, %ecx
cmpl MAX_AGE(%ebp), %ecx
jg save_max_age

jmp read_loop

save_max_age:
movl %ecx, MAX_AGE(%ebp)
jmp read_loop

exit:
close_file:
movl $SYS_CLOSE, %eax
movl ST_IN_FD(%ebp), %ebx
int $SYSCALL

movl $SYS_EXIT, %eax
movl MAX_AGE(%ebp), %ebx
int $SYSCALL
