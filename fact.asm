# PURPOSE: Demo the usage of a factorial function

# no data is used
.section .data

.section .text
.globl _start

_start:
pushl $5 # calling parameter
call fact
addl $4, %esp

# prepare to exit
movl %eax, %ebx
movl $1, %eax
int $0x80

.type fact, @function

# PURPOSE: A function to compute the factorial of its parameter
#
# INPUT: first argument n, factorial to compute to
#
# OUTPUT: the factorial of n
#
# VARIABLES:
#            %eax: current n value, recursive call return value and its own return value
#            %ebx: temporary storage

fact:
pushl %ebp
movl %esp, %ebp
movl 8(%esp), %eax

# when %eax is lower than 1; stop for fact(1) = 1
cmpl $1, %eax
jle fact_end

# recursively call fact(n - 1)
decl %eax
pushl %eax
call fact

# clean the frame and reload param in %ebx
popl %ebx
movl 8(%esp), %ebx

imull %ebx, %eax
jmp fact_end

fact_end:
movl %ebp, %esp
popl %ebp
ret
