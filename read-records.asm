.include "linux.s"
.include "record-def.s"
# PURPOSE: Read records from file and print out to stdout

.section .data
file_name:
.ascii "test.dat\0"

newline:
.byte '\n'

.section .bss
.lcomm BUFFER, RECORD_SIZE

.section .text
.equ ST_FD, -4
.equ ST_STR_SIZE, -8

.global _start
_start:
movl %esp, %ebp
subl $8, %esp # reserve stack space for file descriptor and strlen counter

cmpl $1, ST_ARGC(%ebp)
je open_default

open_file:
movl $SYS_OPEN, %eax
movl ST_ARGV_1(%ebp), %ebx
movl $0, %ecx # O_RDONLY
movl $0666, %edx
int $SYSCALL
movl %eax, ST_FD(%ebp)
jmp read_loop

open_default:
movl $SYS_OPEN, %eax
movl $file_name, %ebx
movl $0, %ecx # O_RDONLY
movl $0666, %edx
int $SYSCALL
movl %eax, ST_FD(%ebp)
jmp read_loop

read_loop:
pushl ST_FD(%ebp)
pushl $BUFFER
call read_record
addl $8, %esp

cmpl $RECORD_SIZE, %eax
jne exit # return when we are not getting a full record

# Count the chars in the string

pushl $RECORD_LASTNAME + BUFFER
call count_char
addl $4, %esp
movl %eax, ST_STR_SIZE(%ebp)

# write record's first name out to stdout
movl $SYS_WRITE, %eax
movl $STDOUT, %ebx
movl $RECORD_LASTNAME + BUFFER, %ecx
movl ST_STR_SIZE(%ebp), %edx
int $SYSCALL

# write new line
movl $SYS_WRITE, %eax
movl $STDOUT, %ebx
movl $newline, %ecx
movl $1, %edx
int $SYSCALL

jmp read_loop

exit:
close_file:
movl $SYS_CLOSE, %eax
movl ST_FD(%ebp), %ebx
int $SYSCALL

movl $SYS_EXIT, %eax
movl $0, %ebx
int $SYSCALL


# PURPOSE: count character in string until a '\0' is reached
#
# INPUT: address of beginning character of the string
# 
# OUTPUT: count of chars in %eax
# 
# VARIABLES:
#       %edi: to store current index into string
#       %ecx: store the beginning of str address

.equ ST_STR_ADDR, 8

.type count_char, @function
count_char:
pushl %ebp
movl %esp, %ebp

movl ST_STR_ADDR(%ebp), %ecx
movl $0, %edi

count_loop:
cmpl $0, (%ecx, %edi, 1)
je count_return

incl %edi
jmp count_loop

count_return:
movl %edi, %eax
movl %ebp, %esp
popl %ebp
ret
