# PURPOSE: This program converts input (only ASCII is allowed) to uppercase and write to output
# 
# PROCEDURES:
#       1) open input and output files, save file descriptor in main program's stacks
#       2) copy data from input to buffer chunk by chunk; then convert data in buffer
#          to uppercase through function call; write the buffer to output file
#       3) continue the above loop until read EOF
#       4) close both input and output files
.include "linux.s"

.section .data

.section .bss
.equ BUFFER_SIZE, 500
.lcomm BUFFER_DATA, BUFFER_SIZE  # why $ reference is not required?

.section .text

# FILE MODE
# Defined in /usr/include/asm-generic/fcntl.h
.equ O_RDONLY, 0
.equ O_CREAT_WRONLY_TRUNC_APPEND, 03101
.equ F_PERM, 0666

# MAIN PROGRAM STACK OFFSET
.equ ST_SIZE_RESERVE, 8   # reserve spaces on main stack for variables
.equ ST_FD_OUT, -8        # var for output file descriptor
.equ ST_FD_IN, -4         # var for input file descriptor

.globl _start

_start:
# initialize the main stacks
movl %esp, %ebp
subl $ST_SIZE_RESERVE, %esp

# use stdin or stdout filenames are not set
cmpl $2, ST_ARGC(%ebp)
jl use_stdin_stdout

# procedure one 
open_files:
open_input:
movl $SYS_OPEN, %eax
movl ST_ARGV_1(%ebp), %ebx
movl $O_RDONLY, %ecx
movl $F_PERM, %edx
int $SYSCALL
movl %eax, ST_FD_IN(%ebp)

open_output:
movl $SYS_OPEN, %eax
movl ST_ARGV_2(%ebp), %ebx
movl $O_CREAT_WRONLY_TRUNC_APPEND, %ecx
movl $F_PERM, %edx
int $SYSCALL
movl %eax, ST_FD_OUT(%ebp)

jmp read_loop

use_stdin_stdout:
movl $STDIN, ST_FD_IN(%ebp)
movl $STDOUT, ST_FD_OUT(%ebp)
jmp read_loop

# read the data into buffer, convert them and write to output
read_loop:
movl ST_FD_IN(%ebp), %ebx
movl $BUFFER_DATA, %ecx
movl $BUFFER_SIZE, %edx
movl $SYS_READ, %eax
int $SYSCALL

# from read(2) linux manual page
# On success, the number of bytes read is returned (zero indicates end
# of file), ...  On error, -1 is returned, ...
cmpl $EOF, %eax
jle exit # return when we reach EOF or SYS_READ is error

pushl $BUFFER_DATA
pushl %eax # Not using $BUFFER_SIZE, because it differs from actual byte read
call convert_to_upper
popl %eax # restore the read size
addl $4, %esp

write_output:
movl ST_FD_OUT(%ebp), %ebx
movl $BUFFER_DATA, %ecx
movl %eax, %edx
movl $SYS_WRITE, %eax
int $SYSCALL

cmpl $0, %eax
je exit # return when there is error

jmp read_loop

exit:
movl $SYS_CLOSE, %eax
movl ST_FD_IN(%ebp), %ebx
int $SYSCALL

movl $SYS_CLOSE, %eax
movl ST_FD_OUT(%ebp), %ebx
int $SYSCALL

movl $SYS_EXIT, %eax
movl $0, %ebx
int $SYSCALL

# PURPOSE: convert character data to uppercase
# 
# ARGUMENTS:
#       arg_1: the beginning address of the data
#       arg_2: length of the data
# 
# VARIABLES:
#       %eax: beginning address of data, from arg_1
#       %ebx: length of data, from arg_2
#       %edi: current offest in data
#       %cl:  current byte under examination
#
# RETURN:
#       0: when success

.type convert_to_upper, @function

.equ LOWERCASE_A, 'a'
.equ LOWERCASE_Z, 'z'
.equ UPPER_CONVERTION, 'A' - 'a'

# LOCAL STACK REFERENCE
.equ DATA_ADDR, 12
.equ DATA_LEN, 8

convert_to_upper:
pushl %ebp
movl %esp, %ebp

movl DATA_ADDR(%ebp), %eax
movl DATA_LEN(%ebp), %ebx
movl $0, %edi

# return if data length equals zero
cmpl $0, %ebx
je end_convert

convert_loop:
movb (%eax, %edi, 1), %cl

cmpb $LOWERCASE_A, %cl
jl next_byte
cmpb $LOWERCASE_Z, %cl
jg next_byte

addb $UPPER_CONVERTION, %cl
movb %cl, (%eax, %edi, 1)

next_byte:
incl %edi
cmpl %edi, %ebx
jne convert_loop # continue unless reaches the end of data length

end_convert:
movl $0, %eax  # return 0 when success
movl %ebp, %esp
popl %ebp
ret
