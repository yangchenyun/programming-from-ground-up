# PURPOSE: Write "hey diddle diddle" to a file called "heynow.txt"
.include "linux.s"

.section .data
output_filename:
.string "heynow.txt"
msg:
# https://www.ece.cmu.edu/~ee349/f-2012/lab2/gas-tips.pdf
.string "hey diddle diddle\n"
.set    msg_size, . - msg

.section .bss
.lcomm output_fd, 4

.section .text

# FILE MODE
# Defined in /usr/include/asm-generic/fcntl.h
.equ O_RDONLY, 0
.equ O_CREAT_WRONLY_TRUNC_APPEND, 03101
.equ F_PERM, 0666

.global _start

_start:
open_file:
movl $SYS_OPEN, %eax
movl $output_filename, %ebx
movl $O_CREAT_WRONLY_TRUNC_APPEND, %ecx
movl $F_PERM, %edx
int $SYSCALL
movl %eax, output_fd  # need the location with address output_fd

write_file:
movl output_fd, %ebx
movl $msg, %ecx       # pass the address itself to system call
movl $msg_size, %edx
movl $SYS_WRITE, %eax
int $SYSCALL

close_file:
movl $SYS_CLOSE, %eax
movl output_fd, %ebx
int $SYSCALL

movl $SYS_EXIT, %eax
movl $0, %ebx
int $SYSCALL
