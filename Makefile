all: find-in-records
	./find-in-records && ./read-records

clean:
	rm -rf *.o exit maxnumber maxnumber-func power fact fact-iter square toupper heynow *-records

find-in-records: find-in-records.o record-io.o
	ld find-in-records.o record-io.o -o find-in-records -m elf_i386

find-in-records.o: find-in-records.asm
	as find-in-records.asm -o find-in-records.o --32

modify-records: modify-records.o record-io.o
	ld modify-records.o record-io.o -o modify-records -m elf_i386

modify-records.o: modify-records.asm
	as modify-records.asm -o modify-records.o --32

read-records: read-records.o record-io.o
	ld read-records.o record-io.o -o read-records -m elf_i386

read-records.o: read-records.asm
	as read-records.asm -o read-records.o --32

write-records: write-records.o record-io.o
	ld write-records.o record-io.o -o write-records -m elf_i386

write-records.o: write-records.asm
	as write-records.asm -o write-records.o --32

record-io.o: record-io.asm
	as record-io.asm -o record-io.o --32

heynow: heynow.o
	ld heynow.o -o heynow -m elf_i386

heynow.o: heynow.asm
	as heynow.asm -o heynow.o --32

toupper: toupper.o
	ld toupper.o -o toupper -m elf_i386

toupper.o: toupper.asm
	as toupper.asm -o toupper.o --32

square: square.o
	ld square.o -o square -m elf_i386

square.o: square.asm
	as square.asm -o square.o --32

exit: exit.o
	ld exit.o -o exit -m elf_i386

exit.o: exit.asm
	as exit.asm -o exit.o --32

fact: fact.o
	ld fact.o -o fact -m elf_i386

fact.o: fact.asm
	as fact.asm -o fact.o --32

fact-iter: fact-iter.o
	ld fact-iter.o -o fact-iter -m elf_i386

fact-iter.o: fact-iter.asm
	as fact-iter.asm -o fact-iter.o --32

maxnumber: maxnumber.o
	ld maxnumber.o -o maxnumber -m elf_i386

maxnumber.o: maxnumber.asm
	as maxnumber.asm -o maxnumber.o --32

maxnumber-func: maxnumber-func.o
	ld maxnumber-func.o -o maxnumber-func -m elf_i386

maxnumber-func.o: maxnumber-func.asm
	as maxnumber-func.asm -o maxnumber-func.o --32

power: power.o
	ld power.o -o power -m elf_i386

power.o: power.asm
	as power.asm -o power.o --32
