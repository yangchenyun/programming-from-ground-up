# PURPOSE: Demo the usage of a factorial function

# no data is used
.section .data

.section .text
.globl _start

_start:
pushl $5 # calling parameter
call fact
addl $4, %esp

# prepare to exit
movl %eax, %ebx
movl $1, %eax
int $0x80

# PURPOSE: A function to compute the factorial of its parameter iteratively
#
# INPUT: first argument n, factorial to compute to
#
# OUTPUT: the factorial of n
#
# VARIABLES:
#            %eax: current result and final return value
#            %ebx: current n value

.type fact, @function
fact:
pushl %ebp
movl %esp, %ebp
movl 8(%esp), %ebx
movl $1, %eax

fact_loop:
cmpl $1, %ebx
jle fact_end
imull %ebx, %eax
decl %ebx
jmp fact_loop

fact_end:
movl %esp, %ebp
popl %ebp
ret
