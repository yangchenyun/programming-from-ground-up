.include "linux.s"
.include "record-def.s"

.equ ST_FD, 12
.equ ST_BUFFER, 8

# PURPOSE: Function to read one record from file into buffer
#
# ARGUMENTS:
#       arg_1: file descriptor
#       arg_2: buffer

.section .text
.global read_record
.type read_record, @function
read_record:
pushl %ebp
movl %esp, %ebp

movl $SYS_READ, %eax
movl ST_FD(%ebp), %ebx
movl ST_BUFFER(%ebp), %ecx
movl $RECORD_SIZE, %edx
int $SYSCALL

# %eax is returned as the read status
movl %ebp, %esp
popl %ebp
ret

# PURPOSE: Function to write one record to file from buffer
#
# ARGUMENTS:
#       arg_1: file descriptor
#       arg_2: buffer

.section .text
.global write_record
.type write_record, @function
write_record:
pushl %ebp
movl %esp, %ebp

movl $SYS_WRITE, %eax
movl ST_FD(%ebp), %ebx
movl ST_BUFFER(%ebp), %ecx
movl $RECORD_SIZE, %edx
int $SYSCALL

# %eax is returned as the write status
movl %ebp, %esp
popl %ebp
ret
