.include "linux.s"

# PURPOSE: Program to write three data records into a file named "record.data"

.section .data
#Constant data of the records we want to write
#Each text data item is padded to the proper
#length with null (i.e. 0) bytes.

record1:
.ascii "Fredrick\0"
.rept 31 #Padding to 40 bytes
.byte 0
.endr

.ascii "Bartlett\0"
.rept 31 #Padding to 40 bytes
.byte 0
.endr

.ascii "4242 S Prairie\nTulsa, OK 55555\0"
.rept 209 #Padding to 240 bytes
.byte 0
.endr

.ascii "+1847312341\0"
.rept 28 #Padding to 40 bytes
.byte 0
.endr

.long 45

record2:
.ascii "Marilyn\0"
.rept 32 #Padding to 40 bytes
.byte 0
.endr

.ascii "Taylor\0"
.rept 33 #Padding to 40 bytes
.byte 0
.endr

.ascii "2224 S Johannan St\nChicago, IL 12345\0"
.rept 203 #Padding to 240 bytes
.byte 0
.endr

.ascii "+1941312341\0"
.rept 28 #Padding to 40 bytes
.byte 0
.endr

.long 29

record3:
.ascii "Derrick\0"
.rept 32 #Padding to 40 bytes
.byte 0
.endr
.ascii "McIntire\0"
.rept 31 #Padding to 40 bytes
.byte 0
.endr

.ascii "500 W Oakland\nSan Diego, CA 54321\0"
.rept 206 #Padding to 240 bytes
.byte 0
.endr

.ascii "+86941312341\0"
.rept 27 #Padding to 40 bytes
.byte 0
.endr

.long 36

#This is the name of the file we will write to
file_name:
.ascii "test.dat\0"

.section .text

.equ ST_FD, -4

.global _start
_start:
movl %esp, %ebp
subl $4, %esp # reserve stack space for file descriptor

open_file:
movl $SYS_OPEN, %eax
movl $file_name, %ebx
movl $0101, %ecx  # O_WRONLY & O_CREAT
movl $0666, %edx
int $SYSCALL
movl %eax, ST_FD(%ebp)

write_records:
pushl ST_FD(%ebp)
pushl $record1
call write_record
addl $8, %esp

pushl ST_FD(%ebp)
pushl $record2
call write_record
addl $8, %esp

pushl ST_FD(%ebp)
pushl $record3
call write_record
addl $8, %esp

close_file:
movl $SYS_CLOSE, %eax
movl ST_FD(%ebp), %ebx
int $SYSCALL

movl $SYS_EXIT, %eax
movl $0, %ebx
int $SYSCALL
