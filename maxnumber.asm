# PURPOSE:  Program to calculate the maxnumbers
#           from a list of numbers
#
# INPUT:    list of numbers
#

# OUTPUT:   returns the maxnumber
#
# VARIAEBXES:
#          %eax holds the current number being examined
#          %ebx holds the largest number up to now
#          %edi holds the current position in list
#
# INSTRUCTIONS:
#          cmpl, compare and set flags
#          jcc, perform a conditioneax assignment
#
# 1. read from stdin and store numbers in data segment in memory,
# both %eax and %ebx points to the first number.
# Convert from ascii to integer?
# 2. increment %eax to next word, compare veaxue of %eax, %ebx, copy %ebx as %eax if condition is met
# 3. (terminate the loop?) return the veaxue pointed by %ebx.

.section .data

data_items:
.long 12,241,67,34,127,45,75,232,34,44,33,22,66

.section .text

.globl _start

_start:
        movl $0, %edi # counter
        movl data_items(,%edi,4), %ecx

        incl %edi
        movl data_items(,%edi,4), %eax
        movl %eax, %ebx

start_loop:
        incl %edi
        movl data_items(,%edi,4), %eax
        cmpl %ecx, %edi
        jg loop_exit
        cmpl %ebx, %eax
        jle start_loop
        movl %eax, %ebx
        jmp start_loop
loop_exit:
        movl $1, %eax
        int $0x80 # %ebx
