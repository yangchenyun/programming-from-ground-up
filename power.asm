# Purpose: Program to illustrate how functions work

# No data is needed for this program
.section .data

.section .text

.globl _start
_start:

pushl $4  # push second param
pushl $3  # push first param
call power
addl $8, %esp # restore the stack pointer back

# call exit with %ebx as return value
movl %eax, %ebx 
movl $1, %eax
int $0x80

# PURPOSE: Program to compute a value by raise a number to a power
#
# INPUT:   First argument - n base number
#          Second argument - m power it raises to
#
# OUTPUT:  Return the value of n^m
#
# VARIABLES:
#          %ebx: holds the base number
#          %ecx: holds the power
#          %eax: temporary storage and return value

.type power, @function # the annotation a function name as address

power:
# save %ebp as current stack pointer value
pushl %ebp
movl %esp, %ebp
subl $4, %esp

movl 8(%ebp), %ebx  # just for convenience and speed (register is faster)
movl 12(%ebp), %ecx 
movl %ebx, %eax

cmpl $0, %ecx
je power_zero

power_loop_start:
cmpl $1, %ecx
je power_end
imull %ebx, %eax # use %eax to hold the value
decl %ecx
jmp power_loop_start

power_zero:
movl $1, %eax
movl %ebp, %esp # restore %esp to %ebp
popl %ebp       # pop %ebp off
ret

power_end:
movl %ebp, %esp # restore %esp to %ebp
popl %ebp       # pop %ebp off
ret
