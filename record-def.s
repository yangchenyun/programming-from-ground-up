# define field offset from the beginning of a record
.equ RECORD_FIRSTNAME, 0
.equ RECORD_LASTNAME, 40
.equ RECORD_ADDRESS, 80
.equ RECORD_PHONE, 320
.equ RECORD_AGE, 360

.equ RECORD_SIZE, 364
