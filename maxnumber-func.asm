# PURPOSE:  Use function to calculate the maxnumbers
#           from a list of numbers
#
# INPUT:    list of 9 numbers
#
# OUTPUT:   returns the maxnumber
#
# VARIAEBXES:

.section .data

data_items:
.long 12,241,67,34,127,45,75,232,34,44,33,22,66

.section .text

.globl _start

_start:

# push the data address of data section on to stack
# in order to copy the data_items as an address instead of reference it
# we do it manually
subl $4, %esp
leal data_items, %esp
call max
addl $4, %esp

movl %eax, %ebx
movl $1, %eax
int $0x80


# PURPOSE:  function to calculate max number from
#           three numbers
#
# INPUT:    pointer to a list of values, the first value is the length of list
#
# OUTPUT:   returns the maxnumber
#
# VARIAEBXES:
#               %eax: stores current max number
#               %ebx: stores the current number under comparison
#               %ecx: stores the pointer to the list
#               %edi: index to access the data

.type max, @function
max:
pushl %ebp
movl %esp, %ebp

# load the value at address 8(%ebp) at %ecx, which is the address of
# the beginning of list but don't know how to calculate offset based
# on address on value in a register TODO: why access to (%ecx,%edi,4)
# isn't valid?  The data reference looks good:
# https://banisterfiend.wordpress.com/2008/08/15/calling-an-asm-function-from-c/
# movl 8(%ebp), %ecx

xorl %edi, %edi
movl data_items(,%edi,4), %ebx
movl %ebx, %eax

max_loop:
incl %edi
cmpl data_items, %edi # test for end of list
je max_end
movl data_items(,%edi,4), %ebx
cmpl %eax, %ebx
jle max_loop
movl %ebx, %eax
jmp max_loop

max_end:
movl %ebp, %esp
popl %ebp
ret
